## Proyecto de prueba:

En proyecto de prueba de maquetación se ha realizado siguiendo BEM, con una estructura
y organización diferentes a la de Jordi, sin utilizar ningún framework de estilos.

Aún así, hemos trabajado conjuntamente en algunos puntos haciendo cada uno su propia nav
bar.

Hemos querido hacer dos proyectos diferentes para que así veais dos maneras de trabajar
como también nuestra capacidad de adaptarnos e improvisar según lo demandado.

## Estructura del proyecto:

- src/assets -> Imágenes del proyecto
- src/components -> Componentes del proyecto
  - src/components/NavBar.vue -> Componente creado siguiendo la estructura BEM
  - src/components/NavBar.scss -> Componente de estilos creado siguiendo la estructura BEM

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
